import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('HAYA/Login/InputUsuario'), GlobalVariable.Usuario)

WebUI.setText(findTestObject('HAYA/Login/InputContrasena'), GlobalVariable.Contrasena)

WebUI.click(findTestObject('HAYA/Login/Validar'))

WebUI.verifyElementAttributeValue(findTestObject('HAYA/Menu/BotonTareas'), 'class', 'x-treelist-item-tool app-menu-ico ico-diary x-treelist-item-selected', 
    0)

WebUI.click(findTestObject('HAYA/Menu/BotonActivos'))

WebUI.verifyElementPresent(findTestObject('HAYA/Activos/TextoFiltroActivos'), GlobalVariable.TiempoEspera)

WebUI.setText(findTestObject('HAYA/Activos/InputActivo'), NumeroActivoText)

WebUI.click(findTestObject('HAYA/Activos/BuscarActivos'))

WebUI.waitForElementPresent(findTestObject('HAYA/Activos/TablaActivos'), GlobalVariable.TiempoEspera)

WebUI.doubleClick(findTestObject('HAYA/Activos/TablaActivos'))

WebUI.waitForElementVisible(findTestObject('HAYA/Activos/ImgActivo'), GlobalVariable.TiempoEspera)

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/Aviso'))

color = WebUI.getCSSValue(findTestObject('HAYA/Activos/Aviso'), 'background-color')

WebUI.verifyEqual(Color, color)

WebUI.verifyElementText(findTestObject('HAYA/Activos/TextoCartera'), Cartera)

WebUI.waitForElementPresent(findTestObject('HAYA/Activos/ImagenIcono'), GlobalVariable.TiempoEspera)

WebUI.verifyElementText(findTestObject('HAYA/Activos/TextoSubcartera'), Subcartera)

WebUI.verifyElementPresent(findTestObject('HAYA/Activos/DatosBasicos/Identificacion'), GlobalVariable.TiempoEspera)

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/DescripcionActivo'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/EstadoFActivo'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/UsoDominante'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroHaya'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroPrinex'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroRecovery'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroRem'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroSareb'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/NumeroUvem'))

WebUI.verifyElementVisible(findTestObject('HAYA/Activos/DatosBasicos/SubtipoActivo'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('HAYA/Activos/DatosBasicos/BDNumeroHaya'), NumeroActivoText)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('HAYA/Activos/DatosBasicos/BDTipoActivo'), TipoActivoText)

WebUI.click(findTestObject('HAYA/Activos/SituacionPosesoriaLlaves/PestanaSituacionPosesoriaLlaves'))

WebUI.click(findTestObject('HAYA/Activos/TituloEInformacionRegistral/PestanaTituloEInformacionRegistral'))


