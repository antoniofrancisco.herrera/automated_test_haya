import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('HAYA/Login/InputUsuario'), GlobalVariable.Usuario)

WebUI.setText(findTestObject('HAYA/Login/InputContrasena'), GlobalVariable.Contrasena)

WebUI.click(findTestObject('HAYA/Login/Validar'))

WebUI.verifyElementAttributeValue(findTestObject('HAYA/Menu/BotonTareas'), 'class', 'x-treelist-item-tool app-menu-ico ico-diary x-treelist-item-selected', 
    0)

WebUI.click(findTestObject('HAYA/Menu/BotonAgrupaciones'))

WebUI.waitForElementPresent(findTestObject('HAYA/Agrupaciones/Loadingagrupaciones'), GlobalVariable.TiempoEspera)

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/ListadoAgrupaciones'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FiltroAgrupaciones'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('HAYA/Agrupaciones/Input_Agrupaciones'), Agrupacion)

WebUI.waitForElementVisible(findTestObject('HAYA/Agrupaciones/Buscar'), GlobalVariable.TiempoEspera)

WebUI.click(findTestObject('HAYA/Agrupaciones/Buscar'))

WebUI.waitForElementVisible(findTestObject('HAYA/Agrupaciones/Tablerecord'), GlobalVariable.TiempoEspera)

WebUI.delay(2)

WebUI.doubleClick(findTestObject('HAYA/Agrupaciones/Tablerecord'))

WebUI.waitForElementPresent(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/TabAgrupacion'), GlobalVariable.TiempoEspera)

WebUI.waitForElementClickable(findTestObject('HAYA/Agrupaciones/Peticiontrabajo'), GlobalVariable.TiempoEspera)

WebUI.click(findTestObject('HAYA/Agrupaciones/Peticiontrabajo'))

WebUI.waitForElementVisible(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/VePeticionTrabajo'), GlobalVariable.TiempoEspera)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/ComboTipoTrabajo'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/TipoTrabajo'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/ComboSubtipoTrabajo'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/SubtipoTrabajo'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/ComboGestoraActual'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/GestoraActual'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/ComboSupervisorActual'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/SupervisorActual'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/BotonCrear'))

WebUI.verifyTextPresent(ConfirmarTrabajos, false)

WebUI.click(findTestObject('HAYA/Agrupaciones/AgrupacionesTrabajo/BotonSi'))

WebUI.waitForElementPresent(findTestObject('HAYA/Agrupaciones/LoadingTrabajo'), GlobalVariable.TiempoEspera)

WebUI.delay(20, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('HAYA/Agrupaciones/BotonEditar'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoDestinoComercial'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoDescripcion'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoCartera'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoNombre'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoDireccion'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoMunicipio'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoProvincia'))

WebUI.verifyElementVisible(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ElementoCodigoPostal'))

WebUI.setText(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/Input_Direccion'), Direccion)

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ComboProvincia'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/SelectProvincia'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ComboDestinoComercial'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/SelectDestinoComercial'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ComboMunicipio'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/SelectMunicipio'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/ComboCartera'))

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/SelectCartera'))

WebUI.delay(1)

WebUI.setText(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/Input_Nombre'), Nombre)

WebUI.delay(1)

WebUI.setText(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/Input_Descripcion'), Descripcion)

WebUI.delay(1)

WebUI.setText(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/Input_CodigoPostal'), CodigoPostal)

WebUI.delay(1)

WebUI.click(findTestObject('HAYA/Agrupaciones/BotonModificar'))

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarDireccion'), 5)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarProvincia'), 5)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarMunicipio'), 5)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarNombre'), 5)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarDescripcion'), 5)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/FichaAgrupaciones/VerificarCodigoPostal'), 5)

WebUI.delay(5)

WebUI.click(findTestObject('HAYA/Agrupaciones/ListadoActivos'))

listado = WebUI.getText(findTestObject('HAYA/Agrupaciones/ListadoActivosAgrupaciones/InputActivosIncluidos'))

WebUI.verifyNotMatch('1', listado, false)

WebUI.click(findTestObject('HAYA/Agrupaciones/Subdivisiones'))

WebUI.waitForElementVisible(findTestObject('HAYA/Agrupaciones/Subdivisiones/LoadingSubdivisiones'), GlobalVariable.TiempoEspera)

WebUI.verifyElementPresent(findTestObject('HAYA/Agrupaciones/Subdivisiones/SubdivisionesItems'), 5)

