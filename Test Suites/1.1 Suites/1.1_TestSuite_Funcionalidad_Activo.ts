<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.1_TestSuite_Funcionalidad_Activo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bea16caf-81d6-4250-9770-8258912da6d4</testSuiteGuid>
   <testCaseLink>
      <guid>122a9f1a-7d6d-482e-a112-c4878c7beebc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1.1 Cases/Test_Case-HAYA-1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>97a7a88e-8aca-45ad-a337-f4cf17ee442f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/1.1 Data/Test_Case_HAYA-1_dataVAL3</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>97a7a88e-8aca-45ad-a337-f4cf17ee442f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>activo</value>
         <variableId>c0aace8e-35e5-43b8-84c9-f6c94a38b212</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>97a7a88e-8aca-45ad-a337-f4cf17ee442f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tipo activo</value>
         <variableId>8193b1f2-04be-4e2f-96b3-d17744408dc3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>255745be-5c5f-4cac-bef9-289f6d07b5f7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>727cf04a-8512-4aa8-bd04-72792af076e3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>03e7125c-30f4-4ec9-8753-d0243960ae66</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
