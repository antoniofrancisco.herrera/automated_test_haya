<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Inmobiliario</name>
   <tag></tag>
   <elementGuidId>fdf4a6ad-d82d-4247-a300-50d530b8e6c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'displayfield-') and contains(@id, '-inputEl') and text() = '${TIPO_ACTIVO}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
