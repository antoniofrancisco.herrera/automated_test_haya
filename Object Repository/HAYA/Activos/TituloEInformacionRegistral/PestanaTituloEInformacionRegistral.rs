<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PestanaTituloEInformacionRegistral</name>
   <tag></tag>
   <elementGuidId>c8667225-f147-4a28-ba75-c5a71e81d5fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[starts-with(@id, 'tab-') and contains(@id, '-btnInnerEl') and text() = 'Título e información registral']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
