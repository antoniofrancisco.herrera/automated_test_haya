<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TablaActivos</name>
   <tag></tag>
   <elementGuidId>a21cdbe5-727a-4fd3-81bc-42eeba235231</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'activosmain-')]//table[starts-with(@id,'tableview-') and contains(@id,'-record-')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
