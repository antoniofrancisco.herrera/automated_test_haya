<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NumeroPrinex</name>
   <tag></tag>
   <elementGuidId>50fcadc3-8621-4712-82fd-40c18b2426e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'datosbasicosactivo-') and contains(@id, '-innerCt')]//label[starts-with(@id,'displayfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Nº activo PRINEX:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
