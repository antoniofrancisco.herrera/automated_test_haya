<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BDTipoActivo</name>
   <tag></tag>
   <elementGuidId>fac8acfb-3771-49f2-994b-24743bc73cc9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id,'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Tipo de activo:']/../following-sibling::*[1]//div[starts-with(@id,'ext-element')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
