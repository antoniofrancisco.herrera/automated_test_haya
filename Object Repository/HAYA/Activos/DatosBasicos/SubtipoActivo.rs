<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SubtipoActivo</name>
   <tag></tag>
   <elementGuidId>07ceab5d-2b12-47cf-8dd9-ad31ff70bd9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'datosbasicosactivo-') and contains(@id, '-innerCt')]//label[starts-with(@id,'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Subtipo de activo:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
