<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NumeroSareb</name>
   <tag></tag>
   <elementGuidId>f3a1ab82-7037-4ddb-bde9-14f2c40747ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'datosbasicosactivo-') and contains(@id, '-innerCt')]//label[starts-with(@id,'displayfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Nº activo SAREB:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
