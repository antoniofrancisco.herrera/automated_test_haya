<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BDNumeroHaya</name>
   <tag></tag>
   <elementGuidId>bdd2cec3-12cd-4c0a-a977-43e50c030b61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id,'displayfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Nº activo HAYA:']/../following-sibling::*[1]//div[starts-with(@id,'displayfieldbase-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
