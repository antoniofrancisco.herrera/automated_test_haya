<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DescripcionActivo</name>
   <tag></tag>
   <elementGuidId>0e931ae9-44cd-4624-80c1-22dfbde95e49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'datosbasicosactivo-') and contains(@id, '-innerCt')]//label[starts-with(@id,'textareafieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Breve descripción del activo:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
