<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TipoActivo</name>
   <tag></tag>
   <elementGuidId>9517abb2-d452-440d-9945-8dd4883bb3ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'datosbasicosactivo-') and contains(@id, '-innerCt')]//label[starts-with(@id,'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Tipo de activo:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
