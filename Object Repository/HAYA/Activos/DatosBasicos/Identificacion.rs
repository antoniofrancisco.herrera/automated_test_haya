<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Identificacion</name>
   <tag></tag>
   <elementGuidId>05a52365-0a2b-4ff0-be3a-d8e86e785e4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'fieldsettable-') and contains(@id, '-legendTitle') and text() = 'Identificación']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
