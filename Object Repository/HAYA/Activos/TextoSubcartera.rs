<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TextoSubcartera</name>
   <tag></tag>
   <elementGuidId>d3af39d0-f8e8-43f8-bfdd-239c558b90ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id,'displayfield-') and contains(@id,'-labelEl')]/span[text() = 'Subcartera:']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
