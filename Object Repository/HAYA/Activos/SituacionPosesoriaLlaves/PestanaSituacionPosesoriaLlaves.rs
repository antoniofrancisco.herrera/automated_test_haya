<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PestanaSituacionPosesoriaLlaves</name>
   <tag></tag>
   <elementGuidId>b4f11163-bd85-4b52-af00-03709d8444d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[starts-with(@id, 'tab-') and contains(@id, '-btnInnerEl') and text() = 'Situación posesoria y llaves']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
