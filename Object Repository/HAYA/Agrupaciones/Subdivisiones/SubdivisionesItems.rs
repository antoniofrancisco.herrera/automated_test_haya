<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SubdivisionesItems</name>
   <tag></tag>
   <elementGuidId>33b088ed-d717-48cb-91c9-cdbbe2bd19b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'gridBase-') and contains(@id, '-body')]//table[starts-with(@id, 'tableview-') and contains(@id, '-record-')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
