<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LoadingSubdivisiones</name>
   <tag></tag>
   <elementGuidId>53fd750a-309a-4ccd-81aa-2b4b8620f6d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'subdivisionesagrupacion-') and contains(@id, '-body')]//div[starts-with(@id, 'loadmask-') and contains(@id, '-msgWrapEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
