<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ComboSubtipoTrabajo</name>
   <tag></tag>
   <elementGuidId>b8749e61-5731-4814-8296-10e1ed8b762c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Subtipo de trabajo:']/../following-sibling::*[1]//div[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-trigger-picker')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
