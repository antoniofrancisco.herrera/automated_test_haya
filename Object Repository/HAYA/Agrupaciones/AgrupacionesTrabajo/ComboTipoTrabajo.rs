<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ComboTipoTrabajo</name>
   <tag></tag>
   <elementGuidId>a0ececa8-f1ab-4ce3-b9fa-b16523d0fadd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Tipo de trabajo:']/../following-sibling::*[1]//div[starts-with(@id,'comboboxfieldbase-') and contains(@id,'-trigger-picker')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
