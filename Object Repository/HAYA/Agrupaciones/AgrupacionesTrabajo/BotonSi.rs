<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BotonSi</name>
   <tag></tag>
   <elementGuidId>23d413ee-da00-4ba3-af0e-38bdb6811e22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[starts-with(@id, 'button-') and contains(@id, '-btnInnerEl') and text() = 'Sí']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
