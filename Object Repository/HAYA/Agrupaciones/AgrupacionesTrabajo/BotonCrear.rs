<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BotonCrear</name>
   <tag></tag>
   <elementGuidId>2f742e3b-2b98-433b-ae4b-cc6e65b0ee7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[starts-with(@id, 'button-') and contains(@id, '-btnInnerEl') and text() = 'Crear']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
