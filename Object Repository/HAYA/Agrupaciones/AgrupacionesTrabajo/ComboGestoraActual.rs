<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ComboGestoraActual</name>
   <tag></tag>
   <elementGuidId>46cf3a08-47ef-4bf4-b8a0-ea4e5c1f1f4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Gestor actual del activo:']/../following-sibling::*[1]//div[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-trigger-picker')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
