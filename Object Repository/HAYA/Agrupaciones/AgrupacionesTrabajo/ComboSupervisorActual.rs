<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ComboSupervisorActual</name>
   <tag></tag>
   <elementGuidId>ebb8c4cf-1afd-435c-a772-68d6b4f3c6fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Supervisor actual del activo:']/../following-sibling::*[1]//div[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-trigger-picker')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
