<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Subdivisiones</name>
   <tag></tag>
   <elementGuidId>f13f6513-bb31-4cbd-b407-8140d513f879</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'tabbar-') and contains(@id, '-innerCt')]//span[starts-with(@id, 'tab-') and contains(@id, '-btnInnerEl') and text()= 'Subdivisiones']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
