<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>InputActivosIncluidos</name>
   <tag></tag>
   <elementGuidId>18b1241c-465f-4b96-b51d-5e4f69b44b18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[starts-with(@id, 'displayfield-') and contains(@id, '-labelEl')]/span[text() = 'Activos incluidos:']/../following-sibling::*[1]//div[starts-with(@id, 'displayfield-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
