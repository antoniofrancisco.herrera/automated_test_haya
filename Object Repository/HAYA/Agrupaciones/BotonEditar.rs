<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BotonEditar</name>
   <tag></tag>
   <elementGuidId>e9765a6e-c312-4eb6-ba6d-d532a6caced9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'tabbar-') and contains(@id, '-innerCt')]//span[starts-with(@id, 'buttontab-') and contains(@id, '-btnIconEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
