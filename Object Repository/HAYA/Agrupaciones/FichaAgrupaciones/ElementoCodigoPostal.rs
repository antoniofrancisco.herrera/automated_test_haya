<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ElementoCodigoPostal</name>
   <tag></tag>
   <elementGuidId>5c15f18a-4fff-4e41-bdd8-2800a2258164</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'textfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Código postal:']/../following-sibling::*[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
