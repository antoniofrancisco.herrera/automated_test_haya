<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ElementoDestinoComercial</name>
   <tag></tag>
   <elementGuidId>7a812f7f-4e95-43b8-ad2f-0834a5f5f85c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Destino comercial:']/../following-sibling::*[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
