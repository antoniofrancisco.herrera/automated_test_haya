<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_CodigoPostal</name>
   <tag></tag>
   <elementGuidId>4c3d5698-c92b-4acb-95a4-6c05fb4d8e6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'textfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Código postal:']/../following-sibling::*[1]//input[starts-with(@id, 'textfieldbase-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
