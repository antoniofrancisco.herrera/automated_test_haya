<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Direccion</name>
   <tag></tag>
   <elementGuidId>8f7186cf-6763-45f3-b266-f82f2988bd6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'textfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Dirección:']/../following-sibling::*[1]//input[starts-with(@id, 'textfieldbase-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
