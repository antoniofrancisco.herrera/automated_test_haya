<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ElementoCartera</name>
   <tag></tag>
   <elementGuidId>af7ae248-63ec-45f8-9468-dbff3fabd722</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'comboboxfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Cartera:']/../following-sibling::*[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
