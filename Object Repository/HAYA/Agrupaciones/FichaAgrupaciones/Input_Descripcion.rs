<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Descripcion</name>
   <tag></tag>
   <elementGuidId>6903c3fe-700c-4bf0-8a41-395d940fa24b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'textfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Descripción:']/../following-sibling::*[1]//input[starts-with(@id, 'textfieldbase-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
