<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Nombre</name>
   <tag></tag>
   <elementGuidId>c0e80404-7e52-46c6-8a94-f5e274f697f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id, 'agrupacionesdetalle-') and contains(@id, '-body')]//label[starts-with(@id, 'textfieldbase-') and contains(@id, '-labelEl')]/span[text() = 'Nombre:']/../following-sibling::*[1]//input[starts-with(@id, 'textfieldbase-') and contains(@id, '-inputEl')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
