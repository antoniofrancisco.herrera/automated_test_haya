import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/1.1_TestSuite_Funcionalidad_Activo_VAL3')

suiteProperties.put('name', '1.1_TestSuite_Funcionalidad_Activo_VAL3')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\daniel.grisalvo\\Katalon Studio\\ReconversionHAYA\\Reports\\1.1_TestSuite_Funcionalidad_Activo_VAL3\\20190329_110451\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/1.1_TestSuite_Funcionalidad_Activo_VAL3', suiteProperties, [new TestCaseBinding('Test Cases/Test_Case-HAYA-1 - Iteration 1', 'Test Cases/Test_Case-HAYA-1',  [ 'NumeroActivoText' : '64673' , 'TipoActivoText' : 'Vivienda' ,  ]), new TestCaseBinding('Test Cases/Test_Case-HAYA-1 - Iteration 2', 'Test Cases/Test_Case-HAYA-1',  [ 'NumeroActivoText' : '64681' , 'TipoActivoText' : 'Vivienda' ,  ])])
